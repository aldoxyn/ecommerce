import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, ScrollView, ToastAndroid } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Container, Content, Header, Left, Button, Icon, Body, Title, Card, CardItem, Right, Toast } from 'native-base';
import { connect } from 'react-redux';
import { getUser } from '../../src/redux/action/user';
import { TouchableOpacity } from 'react-native-gesture-handler';

class ProfileScreen extends Component {

    componentDidMount() {
        this.props.get_User(this.props.auth.token)
        console.log('kasih tulisan dong', this.props.auth.token)
    };

    render() {
        console.log('user data', this.props.user.data)
        return (
            <ScrollView>
                <Container>
                    <Header style={{ backgroundColor: '#00acee' }}
                        androidStatusBarColor="#00acee">
                        <Left>
                            <Button transparent
                                onPress={() => { this.props.navigation.goBack() }}>
                                <Icon name='arrow-back' />
                            </Button>
                        </Left>
                        <Body>
                            <Title style={styles.titleheader}>Profile</Title>
                        </Body>
                    </Header>
                    <Content>
                        <Card>
                            <CardItem>
                                <View style={{ flexDirection: 'row' }}>
                                    <Body>
                                        <Image
                                            style={styles.imageProfile}
                                            source={{ uri: this.props.user.data.profPict ? this.props.user.data.profPict : null }}
                                        />
                                    </Body>
                                    <View style={{ flexDirection: 'column', flex: 2 }}>
                                        <Text style={styles.textUsername}>{this.props.user.data.username}</Text>
                                        <Text style={styles.textRole}>{this.props.user.data.role}</Text>
                                    </View>
                                </View>
                            </CardItem>
                        </Card>
                        <Card>
                            <CardItem>
                                <View style={{ flexDirection: 'row', flex: 1 }}>
                                    <Left>
                                        <Button transparent onPress={() => { this.props.navigation.navigate('AddProductm') }}>
                                            <Text style={{ color: '#00acee'}}>Add Product</Text>
                                        </Button>
                                    </Left>
                                    <Body>
                                        <Button transparent onPress={() => { this.props.navigation.navigate('HistoryScreenm')}}>
                                            <Text>History Order</Text>
                                        </Button>
                                    </Body>
                                    <Right>
                                        <Button transparent onPress={() => { this.props.navigation.navigate('DeleteProductm') }}>
                                            <Text style={{color: 'red'}}>Delete Product</Text>
                                        </Button>
                                    </Right>
                                </View>
                            </CardItem>
                        </Card>
                    </Content>
                </Container>
            </ScrollView>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user,
});

const mapDispatchToProps = dispatch => {
    return {
        get_User: token => dispatch(getUser(token)),
    };
};

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(ProfileScreen));

const styles = StyleSheet.create({
    titleheader: {
        fontWeight: 'bold',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageProfile: {
        width: 60,
        height: 60,
        flex: 1,
    },
    textUsername: {
        fontWeight: 'bold',
        fontSize: 20,
    },
    textRole: {
        fontSize: 20,
    },
});