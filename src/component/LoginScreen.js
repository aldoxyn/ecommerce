import React, { Component } from 'react';
import { View, ToastAndroid, StyleSheet } from 'react-native';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage';
import { Item, Button, Container, Header, Left, Body, Title, Icon, Label, Input, Text, TouchableOpacity, Content, Card, CardItem, Right, Form } from 'native-base';
import { connect } from 'react-redux';
import { auth } from '../redux/action/AuthAction';
import { withNavigation } from 'react-navigation';

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
    }
  }

  async login() {
    try {
      const postDataLogin = async (bodyParameter) => await axios.post(
        `https://app-ecommerceapps.herokuapp.com/users/login`, bodyParameter, {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      )
      postDataLogin({ 
        username: this.state.username, 
        password: this.state.password 
      })
        .then(response => {
          AsyncStorage.setItem('@token', response.data.result)
          this.props.FetchToken(response.data.result)
          ToastAndroid.show('Login success', ToastAndroid.SHORT)
          this.setState({
            username: '',
            password: ''
          })
          this.props.navigation.navigate('MainScreenm')
        })
        .catch(err => {
          ToastAndroid.show('Sign In Failed', ToastAndroid.SHORT);
          console.log(err);
        })
    }
    catch (e) {
      console.log(e)
    }
  }

  componentDidMount() {
    console.log('auth using redux', this.props.auth);
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#00acee' }}
          androidStatusBarColor="#00acee">
          <Left>
            <Button transparent
              onPress={() => { this.props.navigation.goBack() }}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={styles.titleheader}>Sign In</Title>
          </Body>
          <Right>
          <Button transparent
            onPress={() => this.props.navigation.navigate('SignUpScreenm')}>
            <Text style={styles.titleheader}>Sign Up</Text>
          </Button>
          </Right>
        </Header>
        <View style={{ flex: 1, alignItems: 'center', }}>       
          <Item floatingLabel>
            <Label>
              <Text>Username</Text>
            </Label>
            <Input value={this.state.username} onChangeText={(text) => this.setState({ username: text })} />
          </Item>
          <Item floatingLabel>
            <Label>
              <Text>Password</Text>
            </Label>
            <Input secureTextEntry={true} value={this.state.password} onChangeText={(text) => this.setState({ password: text })} />
          </Item>          
          <Item style={styles.button}>
            <Button transparent
              onPress={() => this.login()}>
              <Text style={styles.text}>Sign In</Text>
            </Button>
          </Item>
          <Text>Forgot Password?</Text>        
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => {
  return {
    FetchToken: token => dispatch(auth(token))
  }
};

export default withNavigation (connect( mapStateToProps, mapDispatchToProps) (LoginScreen));

const styles = StyleSheet.create({
  titleheader: {
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signbutton: {
    fontWeight: 'bold'
  },
  button: {
    display: "flex",
    borderColor: 'transparent',
    margin: 10,
    justifyContent: "center",
    width: 2000,
  },
  text: {
    color: '#00acee',
    fontSize: 20,
    justifyContent: "center",
    alignContent: "center",
    fontWeight: 'bold'
  },
  buttonsignin: {
    margin: 10,
  }
})