import React, { Component } from 'react';
import { View, ToastAndroid, Image, StyleSheet } from 'react-native';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Label, Item, Input, Content, Form, Button, Text, Header, Body, Title } from 'native-base';

class AddProduct extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      price: '',
      stock: '',
      productPict: '',
      photo: null,
    }
  }

  async postProduct() {
    const token = await AsyncStorage.getItem('@token')
    let formData = new FormData();
    formData.append("name", this.state.name)
    formData.append("price", this.state.price)
    formData.append("stock", this.state.stock)
    formData.append("productPict", { uri: this.state.photo.uri, name: this.state.photo.fileName, type: 'image/jpeg' })
    console.log(formData);
    try {
      const addProduct = async (bodyParameter) => await axios.post('https://app-ecommerceapps.herokuapp.com/products/add', bodyParameter,
        {
          headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': `Bearer ${token}`,
          }
        }
      );
      addProduct(formData)
        .then(response => {
          ToastAndroid.show('Add Product Success', ToastAndroid.SHORT)
          console.log(response)
        })
        .catch(err => {
          ToastAndroid.show('Add Product Failed', ToastAndroid.SHORT)
          console.log(err)
        })
    }
    catch (e) {
      console.log(e)
    }
  }

  handleChoosePhoto = () => {
    const option = {
      noData: true
    };
    ImagePicker.launchImageLibrary(option, response => {
      console.log(response);
      if (response.uri) {
        this.setState({ photo: response });
      }
    })
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#00acee' }}>
          <Body>
            <Title>Add Product</Title>
          </Body>
        </Header>
        <View style={{ flex: 1, alignItems: 'center', margin: 10 }}>
          <Item fixedLabel style={styles.formPage}>
            <Input placeholder='Name' value={this.state.name} onChangeText={(text) => this.setState({ name: text })} />
          </Item>
          <Item fixedLabel style={styles.formPage}>
            <Input placeholder='Price' value={this.state.price} onChangeText={(text) => this.setState({ price: text })} />
          </Item>
          <Item fixedLabel style={styles.formPage}>
            <Input placeholder='Stock' value={this.state.stock} onChangeText={(text) => this.setState({ stock: text })} />
          </Item>
          <Item>
            <Button onPress={this.handleChoosePhoto}>
              <Text>Choose Photo</Text>
            </Button>
          </Item>
          <Item>
            <Button onPress={() => this.postProduct()}>
              <Text>Post</Text>
            </Button>
          </Item>
        </View>
      </Container>
    )
  }
}
export default (AddProduct)

const styles = StyleSheet.create({
  formPage: {
    margin: 10,
  },
  text: {
    color: '#00acee',
    fontSize: 20,
    justifyContent: "center",
    alignContent: "center",
    fontWeight: 'bold',
    alignItems: "center",
  },
})