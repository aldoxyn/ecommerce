import React, { Component } from 'react'
import { View, StyleSheet, ToastAndroid } from 'react-native';
import { Item, Button, Container, Header, Left, Body, Title, Icon, Label, Input, Text, TouchableOpacity, Content, Card, CardItem, Right } from 'native-base';
import { withNavigation } from 'react-navigation';
import axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';

class SignUpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      email: '',
      gender: '',
      role: '',
    };
  }

  async signUp() {
    try {
      const signUpData = async (bodyParameter) => await axios.post(
        'https://app-ecommerceapps.herokuapp.com/users/signup', bodyParameter,
        {
          heaaders: {
            'Content-Type': 'application/json'
          }
        })
      signUpData({
        username: this.state.username,
        password: this.state.password,
        email: this.state.email,
        gender: this.state.gender,
        role: 'Customer',
      })
        .then(response => {
          ToastAndroid.show('User successfully saved', ToastAndroid.SHORT)
          this.props.navigation.navigate('Loginm')
          console.log(response);
        })
        .catch(err => {
          ToastAndroid.show('Sign Up Failed', ToastAndroid.SHORT),
            console.log(err)
        })
    }
    catch (e) {
      console.log(e)
    }
  }
  render() {
    return (
      <ScrollView>
        <Container>
          <Header style={{ backgroundColor: '#00acee' }}
            androidStatusBarColor="#00acee">
            <Left>
              <Button transparent
                onPress={() => { this.props.navigation.goBack() }}>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Body>
              <Title style={styles.titleheader}>Sign Up</Title>
            </Body>
          </Header>

          {/* Form Sign Up */}
          <View style={{ flex: 0.75, alignItems: 'center', justifyContent: 'center' }}>
            <Item floatingLabel>
              <Label style={{ color: 'black' }}>Username</Label>
              <Input
                value={this.state.username}
                onChangeText={(text) => this.setState({ username: text })}
              />
            </Item>
            <Item floatingLabel>
              <Label style={{ color: 'black' }}>Password</Label>
              <Input
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={(text) => this.setState({ password: text })}
              />
            </Item>
            <Item floatingLabel>
              <Label style={{ color: 'black' }}>Email Address</Label>
              <Input
                keyboardType="email-address"
                value={this.state.email}
                onChangeText={(text) => this.setState({ email: text })}
              />
            </Item>
            <Item floatingLabel>
              <Label style={{ color: 'black' }}>Gender</Label>
              <Input
                value={this.state.gender}
                onChangeText={(text) => this.setState({ gender: text })}
              />
            </Item>
            {/* <Item floatingLabel>
            <Label style={{ color: 'black' }}>Role</Label>
            <Input
              value={this.state.role}
              onChangeText={(text) => this.setState({ role: text })}
            />
          </Item> */}
            {/* Form Sign Up */}

            <Item style={{ margin: 10 }}>
              <Button
                transparent
                onPress={() => this.signUp()}>
                <Text style={styles.text}>Sign Up</Text>
              </Button>
            </Item>
            <Text>Already have an account?</Text>
            <Button transparent
              style={{ color: 'transparent' }}
              onPress={() => this.props.navigation.navigate('Loginm')}>
              <Text style={styles.signbutton}>Sign In Here</Text>
            </Button>
          </View>
        </Container>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  titleheader: {
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signbutton: {
    fontWeight: 'bold'
  },
  button: {
    display: "flex",
    borderColor: 'transparent',
    margin: 10,
    justifyContent: "center",
    width: 2000,
  },
  text: {
    color: '#00acee',
    fontSize: 20,
    justifyContent: "center",
    alignContent: "center",
    fontWeight: 'bold'
  },
  buttonsignin: {
    margin: 10,
  }
})

export default withNavigation(SignUpScreen);