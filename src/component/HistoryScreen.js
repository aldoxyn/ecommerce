import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { Text, View, StyleSheet, Image, ScrollView, ToastAndroid } from 'react-native';
import { Container, Content, Header, Left, Button, Icon, Body, Title, Card, CardItem, Right, Toast } from 'native-base';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

class HistoryScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: []
    }
  }

  componentDidMount() {
    this.historyOrder()
  }

  historyOrder = async () => {
    const token = await AsyncStorage.getItem('@token')
    try {
      const history = async () => await Axios.get('https://app-ecommerceapps.herokuapp.com/orders', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      history()
        .then(result => {
          this.setState({
            data: result.data.result
          })
          ToastAndroid.show('Done', ToastAndroid.SHORT)
          console.log(result.data)
        })
        .catch(err => {
          ToastAndroid.show('Failed', ToastAndroid.SHORT)
          console.log(err)
        })
    }
    catch (e) {
      console.log(e)
    }
  }

  render() {
    console.log(this.state.data)
    const loops = this.state.data.map(item => {
      return (
        <Card key={item._id}>
          <View style={{ alignItems:'center', justifyContent: 'center'}}>
            <CardItem>
              <Text>{item._id}</Text>
            </CardItem>
            <CardItem>  
              <Text>{item.date}</Text>
            </CardItem>
            <CardItem>
              <Text>{item.total}</Text>
            </CardItem>
          </View>
        </Card>
      )
    })

    return (
      <Container>
        <Content>
          {loops}
        </Content>
      </Container>
    )
  }
}

export default withNavigation(HistoryScreen);