import React from 'react';
import { View, ToastAndroid, StyleSheet } from 'react-native';
import { Item, Button, Container, Header, Left, Body, Title, Icon, Label, Input, Text, TouchableOpacity, Content, Card, CardItem, Right } from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import LoginScreen from './src/component/LoginScreen';
import SignUpScreen from './src/component/SignUpScreen';
import MainScreen from './src/component/MainScreen';
import CartScreen from './src/component/CartScreen';
import ProfileScreen from './src/component/ProfileScreen';
import SignUpSSeller from './src/component/handleSeller/SignUpSSeller';
import AddProduct from './src/component/AddProduct';
import DeleteProduct from './src/component/DeleteProduct';
import HistoryScreen from './src/component/HistoryScreen';
import { Provider } from 'react-redux';
import configureStore from "./store";
const store = configureStore();

class HomeScreen extends React.Component {
  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#00acee' }}
          androidStatusBarColor="#00acee">
          <Body>
            <Title style={styles.titleheader}>E C O M M E R C E</Title>
          </Body>
        </Header>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 35, fontWeight: 'bold' }}>E C O M M E R C E</Text>
          <Text style={{ fontSize: 35, fontWeight: 'bold' }}> </Text>
          <Button transparent
            onPress={() => this.props.navigation.navigate('SignUpScreenm')}>
            <Text style={styles.text}>Create New Account</Text>
          </Button>
          <Item style={styles.button}>
            <Button transparent
              onPress={() => this.props.navigation.navigate('Loginm')} >
              <Text style={styles.text}>Login</Text>
            </Button>
          </Item>
          <Item style={styles.button}>
            <Button rounded style={{ backgroundColor: '#00acee' }}
              onPress={() => this.props.navigation.navigate('SignUpSSellerm')}>
              <Text style={{ fontWeight: 'bold'}}>Create New Account As Seller</Text>
            </Button>
          </Item>
        </View>
      </Container>
    );
  }
}
const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Loginm: {
      screen: LoginScreen
    },
    MainScreenm: {
      screen: MainScreen
    },
    SignUpScreenm: {
      screen: SignUpScreen
    },
    CartScreenm: {
      screen: CartScreen
    },
    ProfileScreenm: {
      screen: ProfileScreen
    },
    SignUpSSellerm: {
      screen: SignUpSSeller
    },
    AddProductm: {
      screen: AddProduct
    },
    DeleteProductm: {
      screen: DeleteProduct
    },
    HistoryScreenm : {
      screen: HistoryScreen
    },
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  titleheader: {
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signbutton: {
    fontWeight: 'bold'
  },
  button: {
    display: "flex",
    borderColor: 'transparent',
    margin: 10,
    justifyContent: "center",
    width: 2000,
  },
  text: {
    color: '#00acee',
    fontSize: 20,
    justifyContent: "center",
    alignContent: "center",
    fontWeight: 'bold'
  },
  buttonsignin: {
    margin: 10,
  }
})

